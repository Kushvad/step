// Our service
$(function() {
    let dataTab = $('[data-tab]');
    dataTab.on('click', function (){
        let data = $(this).data('tab');
        $('.active-tabs').removeClass('active-tabs');
        $('.active-tab').removeClass('active-tab');
        this.classList.add('active-tabs');
        $(data).addClass('active-tab');
    });
})


// Portfolio
$(function() {
    let filter = $('[data-filter]');
    filter.on('click', function() {
        let category = $(this).data('filter');

        if (category === 'load') {
            $('.buttonGreenPlus').remove();
        }

        $('[data-category]').each(function() {
            let currentCategory = $(this).data('category');

            
            if (category === 'all') {
                $(this).removeClass('portfolio-hide');
            } 
            else if (category === 'load'){
                $(this).removeClass('portfolio-hide');
                $(this).removeClass('hidden');
            }
            else {
                if (currentCategory !== category) {
                    $(this).addClass('portfolio-hide')
                }
                else{
                    $(this).removeClass('portfolio-hide');
                }
            } 
        })
    })
})

 
// Slider

$(function() {
    let filterName = $('[data-name]');
    let filterBtn = $('[data-btn]');
    const items = $('.feedback-item');
    const firstItem = items[0];
    const lastItem = items[items.length - 1];
    const images = $('.slider-item');
    const firstImage = images[0];
    const lastImage = images[images.length - 1];


    filterName.on('click', function() {

        let cat = $(this).data('name');
        $('.visible-slider').removeClass('visible-slider');
        $('.active-slider').removeClass('active-slider');
        $(this).addClass('active-slider');
        $(cat).addClass('visible-slider')
    })

    filterBtn.on('click', function() {
        let cat = $(this).data('btn');
        const currentImage = $('.active-slider').get(0);
        const currentItem = $('.visible-slider').get(0);

        if (currentImage !== firstImage && cat === 'prev') {
            $('.active-slider').removeClass('active-slider');    
            $(currentImage).prev().addClass('active-slider'); 
            $('.visible-slider').removeClass('visible-slider');
            $(currentItem).prev().addClass('visible-slider');
        }

        if (currentImage !== lastImage && cat === 'next'){    
            $('.active-slider').removeClass('active-slider');
            $(currentImage).next().addClass('active-slider');
            $('.visible-slider').removeClass('visible-slider');
            $(currentItem).next().addClass('visible-slider');  
        } 
    })   
})

